NEBOAS: a Neutron yiElds Based On AcceleratorS app.

By Miguel MACIAS MARTINEZ at MONNET - European Commission’s Joint Research Center

A web browser application is developed to calculate the double differential neutron yields,
as a function of the energy and angle of neutron emission, from "p", "d", and "alpha"-induced
reactions at any projectile energy on thin and thick targets, for the accelerator-based neutron
sources MONNET (MONo-energetic NEutron Tower) at JRC-Geel.

Reaction cross-sections from ENDF and EXFOR nuclear databases and stopping powers derived with
SRIM2008 code and NIST tables are adopted in the calculation. Integrated neutron yields, neutron
energy spectra, neutron time-of-flight spectra, and angular distribution are obtained as well.

To use the app:
1- download all files in a compressed folder using the download bottom
2- uncompress
3- open the HTML file

Etymology:
<<NEBOAS>> a Galician word from the Latin nebula ("fog"), which resembles a set of neutrons generated around a target to one.